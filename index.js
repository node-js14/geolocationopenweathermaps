require('dotenv').config();
const {
  inquirerMenu,
  leerInput,
  listarLugares,
  pausa,
} = require('./helpers/inquirer');
const { Busquedas } = require('./models/busquedas');
require('colors');

const main = async () => {
  const busquedas = new Busquedas();
  let opt = '';

  do {
    opt = await inquirerMenu();
    switch (opt) {
      case 1:
        // mostrar mensaje
        const termino = await leerInput('Ciudad: ');

        // buscar los lugares
        const lugares = await busquedas.ciudad(termino);

        // seleccionar un lugar
        const idSeleccionado = await listarLugares(lugares);
        if (idSeleccionado === '0') continue;

        const lugarSelect = lugares.find(
          (lugar) => lugar.id === idSeleccionado
        );

        // guardar en db
        busquedas.agregarHistorial(lugarSelect.nombre);

        // consulta por lat y lng
        const latLngConsulta = await busquedas.climaLugar(
          lugarSelect.lat,
          lugarSelect.lng
        );

        console.log('\nInformacion de la ciudad\n'.green);
        console.log('Ciudad: ', lugarSelect.nombre);
        console.log('Lat: ', lugarSelect.lat);
        console.log('Lng: ', lugarSelect.lng);
        console.log('Temperatura: ', latLngConsulta.temp);
        console.log('Minima: ', latLngConsulta.min);
        console.log('Maximo: ', latLngConsulta.max);
        console.log('Descripcion: ', latLngConsulta.desc);

        break;

      case 2:
        busquedas.historialCapitalize.forEach((lugar, i) => {
          const idx = `${i + 1}`.green;
          console.log(`${idx}) ${lugar}`);
        });

        return;
    }

    if (opt !== 0) await pausa();
  } while (opt !== 0);
};

main();
