const inquirer = require('inquirer');
require('colors');

const preguntas = [
  {
    type: 'list',
    name: 'opcion',
    message: '¿Qué desea hacer?',
    choices: [
      {
        value: 1,
        name: '1. Buscar ciudad',
      },
      {
        value: 2,
        name: '2. Historial',
      },
      {
        value: 0,
        name: '0. Salir',
      },
    ],
  },
];

const pausarPreguntas = [
  {
    type: 'input',
    name: 'opcionPreguntar',
    message: `Presione ${'ENTER'.magenta} para continuar \n`,
  },
];

const leerInput = async (message) => {
  const question = [
    {
      type: 'input',
      name: 'desc',
      message,
      validate(value) {
        if (value.length === 0) {
          return 'Por favor ingrese un valor';
        }
        return true;
      },
    },
  ];
  const { desc } = await inquirer.prompt(question);
  return desc;
};

const inquirerMenu = async () => {
  console.clear();
  console.log('======================='.green);
  console.log(' Seleccione una opcion '.green);
  console.log('======================='.green);

  const { opcion } = await inquirer.prompt(preguntas);
  return opcion;
};

const pausa = async () => {
  console.log('\n');
  const { opcionPreguntar } = await inquirer.prompt(pausarPreguntas);
  return opcionPreguntar;
};

const listarLugares = async (lugares = []) => {
  const choices = lugares.map((lugar, index) => {
    return {
      value: lugar.id,
      name: `${index + 1} ${lugar.nombre}`,
    };
  });

  choices.unshift({
    value: '0',
    name: '0. cancelar',
  });

  const preguntas = [
    {
      type: 'list',
      name: 'id',
      message: 'Seleccionar',
      choices,
    },
  ];
  const { id } = await inquirer.prompt(preguntas);
  return id;
};

const confirmar = async (message) => {
  const question = [
    {
      type: 'confirm',
      name: 'ok',
      message,
    },
  ];
  const { ok } = await inquirer.prompt(question);
  return ok;
};

const mostrarListadoCkeckList = async (tareas = []) => {
  const choices = tareas.map((tarea, i) => {
    const idx = `${i + 1}.`.green;

    return {
      value: tarea.id,
      name: `${idx} ${tarea.desc}`,
      checked: tarea.completadoEn ? true : false,
    };
  });

  const pregunta = [
    {
      type: 'checkbox',
      name: 'ids',
      message: 'Selecciones',
      choices,
    },
  ];

  const { ids } = await inquirer.prompt(pregunta);
  return ids;
};

module.exports = {
  inquirerMenu,
  pausa,
  leerInput,
  listarLugares,
  confirmar,
  mostrarListadoCkeckList,
};
