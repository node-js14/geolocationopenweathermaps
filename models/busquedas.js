const fs = require('fs');
const axios = require('axios');

class Busquedas {
  historial = [];
  dbPath = './db/database.json';

  constructor() {
    //    todo leer db si existe
    this.leerDB();
  }

  get historialCapitalize() {
    return this.historial.map((data) => {
      //* codigo para poner en mayuscula la primera letra
      // let palabra;
      // palabra = data.charAt(0).toUpperCase() + data.slice(1);

      //* codigo para poner en mayuscula todas las primeras letras
      let palabra = data.split(' ');
      palabra = palabra.map((p) => p[0].toUpperCase() + p.substring(1));
      // console.log(palabra.join(' '));
      return palabra.join(' ');
    });
  }

  get paramsMapbox() {
    return {
      proximity: 'ip',
      // types: 'place%2Cpostcode%2Caddress',
      language: 'es',
      access_token: process.env.MAPBOX_KEY,
    };
  }

  async ciudad(lugar = '') {
    try {
      // peticion http
      const instance = axios.create({
        baseURL: `https://api.mapbox.com/geocoding/v5/mapbox.places/${lugar}.json`,
        params: this.paramsMapbox,
      });

      const resp = await instance.get();

      return resp.data.features.map((v) => ({
        id: v.id,
        nombre: v.place_name,
        lng: v.center[0],
        lat: v.center[1],
      }));
    } catch (error) {
      return [];
    }
  }

  get paramsOpenWeather() {
    return {
      appid: process.env.OPENWEATHE_KEY,
      units: 'metric',
      lang: 'es',
    };
  }

  async climaLugar(lat, lon) {
    try {
      const consulta = axios.create({
        baseURL: `https://api.openweathermap.org/data/2.5/weather`,
        params: { ...this.paramsOpenWeather, lat, lon },
      });

      const resp = await consulta.get();

      const { weather, main } = resp.data;

      return {
        desc: weather[0].description,
        min: main.temp_min,
        max: main.temp_max,
        temp: main.temp,
      };
    } catch (error) {
      return [];
    }
  }

  agregarHistorial(lugar = '') {
    // TODO prevenir duplicado
    // console.log(lugar);
    if (this.historial.includes(lugar.toLocaleLowerCase())) {
      return;
    }

    // mantener solo 5 busquedas
    this.historial = this.historial.splice(0, 4);

    this.historial.unshift(lugar.toLocaleLowerCase());

    // grabar en db
    this.guardarDB();
  }

  guardarDB() {
    const payload = {
      historial: this.historial,
    };

    fs.writeFileSync(this.dbPath, JSON.stringify(payload));
  }

  leerDB() {
    if (!fs.existsSync(this.dbPath)) {
      return;
    }

    const info = fs.readFileSync(this.dbPath, { encoding: 'utf-8' });
    const data = JSON.parse(info);
    this.historial = data.historial;
  }
}

module.exports = { Busquedas };
